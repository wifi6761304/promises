// Function als async definieren - dadurch können wir auf z. B. einen asynchronen
// fetch request mit der await Syntax arbeiten.
async function fetchData() {
	// Bei async/await werden Fehler über try/catch abgefangen.
	try {
		// Wir können nun auf das Resultat der fetch funktion mit await zugreifen.
		const response = await axios.get('data.json');
		// diese Zeile wird erst ausgeführt, wenn fetch abgearbeit wurde.
		console.log("Success: ", response);
	}
	catch (error) {
		console.log("Error: ", error)
	}
}

fetchData();