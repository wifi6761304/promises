const btn1 = document.getElementById("btn1");
const main = document.querySelector('main');

btn1.addEventListener("click", (e) => {
	axios.get("data.json")
		// resolve
		.then(function (response) {
			// mit fetch muss ich explizit aus der response das json object holen
			// axios liefert mir die Antwort gleich als Objekt
			const data = response.data;
			// data an main anfügen
			const content = data.content;
			main.insertAdjacentHTML('beforeend', content);
		})
		// reject
		.catch(function (error) {
			// handle error
			const p = document.createElement('P');
			p.classList.add('alert', 'alert-warning', 'mt-4');
			p.innerText = error.message;
			main.appendChild(p);
		});
});


