/*
	Ein eigenes Promise Objekt erzeugen:
	Es wird ein callback erwartet. Diese callback Funktion erhält zwei weitere
	callbacks als Parameter. Wir bestimmen durch die interne Logik (hier sehr primitiv),
	wann das erfüllt, und wann es abgelehnt wird.
*/
let promise = new Promise((resolve, reject) => {
	// Platzhalter für z.B. fetch
	let res = 1 + 2;
	if (res === 3) {
		resolve("Success, resolved!");
	} else {
		reject("Oh no, alles falsch!");
	}
});

/*
	Promise wird ausgeführt. Wenn es erfolgreich ist (resolved), kommt es in die Funktion then,
	ansonsten (rejected) wird der catch Block ausgeführt
*/
promise
	// Promise erfüllt, resolved, der angegebene callback wird mit den
	// Parametern aus der resolve Methode ausgeführt
	.then((message) => {
		console.log(`Promise fulfilled: ${message}`);
	})
	.catch((message) => {
		console.log(`Promise rejected: ${message}`);
	});

console.log('Ende Script')